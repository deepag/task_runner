////////////////////////////////////////////////////////
// main prerequisite
////////////////////////////////////////////////////////

var gulp = require('gulp');

////////////////////////////////////////////////////////
// other prerequisites

var sass = require('gulp-sass'),
    ugly = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    minify = require('gulp-minify-css'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    del = require('del'),
    debug = require('gulp-debug'),

    php  = require('gulp-connect-php');

////////////////////////////////////////////////////////
// define tasks
////////////////////////////////////////////////////////

gulp.task( 'sass', function() {
    return gulp.src('app/scss/**/*.scss')
        .pipe(plumber())
        .pipe(sass()
           .on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minify({compatibility: 'ie9'}))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest('app/css/'))
        .pipe(reload({stream: true}));
});

gulp.task( 'bootstrap', function() {
    return gulp.src('bower_components/bootstrap/dist/css/bootstrap.css')
        .pipe(plumber())
        .pipe(concat('bootstrap.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minify({compatibility: 'ie9'}))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulp.dest('app/css/'))
        .pipe(reload({stream: true}));
});

gulp.task('bootstrap:copy', function(){
    return gulp.src('bower_components/bootstrap/dist/**')
        .pipe(gulp.dest('app/'))
});

// Foundation Support - Include in the main task if needed!

gulp.task('foundation:copy', function() {
    return gulp.src('bower_components/foundation/css/foundation.min.css')
        .pipe(gulp.dest('app/css'))
});

gulp.task('foundation:js_copy', function() {
    return gulp.src('bower_components/foundation/js/foundation.min.js')
        .pipe(gulp.dest('app/js'))
});
// foundation default task

gulp.task('foundation', ['foundation:copy', 'foundation:js_copy']);

// End Foundation Support

gulp.task('js', function() {
    return gulp.src('app/scripts/**/*.js')
        .pipe(plumber())
        .pipe(concat('js.js'))
        .pipe(ugly())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/js/'))
        .pipe(reload({stream: true}));

});

gulp.task('jquery', function() {
    return gulp.src('bower_components/jquery/dist/jquery.js')
        .pipe(plumber())
        .pipe(concat('jquery.js'))
        .pipe(ugly())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/js/'))
        .pipe(reload({stream: true}));

});

gulp.task('html', function() {
    return gulp.src('app/**/*.php')
        .pipe(reload({stream: true}));

});

////////////////////////////////////////////////////////
// php-server task
////////////////////////////////////////////////////////

// Serving app/
gulp.task('php', function() {
    php.server({ base: 'app', port: 8000, keepalive: true});
});

//Serving build/
gulp.task('php:build', function() {
    php.server({ base: 'build', port: 8010, keepalive: true});
});

////////////////////////////////////////////////////////
// browser task
////////////////////////////////////////////////////////

// watch app folder
gulp.task('browser-sync', ['php'], function(){
    browserSync({
        proxy: '127.0.0.1:8000',
        port: 8080,
        open: true,
        notify: true
    });
});

// watch build folder
gulp.task('build:serve', ['php:build'], function(){
    browserSync({
        proxy: '127.0.0.1:8010',
        port: 8070,
        open: true,
        notify: false
    });
});

////////////////////////////////////////////////////////
// build tasks
////////////////////////////////////////////////////////


gulp.task('build:copy', function(){
    return gulp.src('app/**/*')
        .pipe(gulp.dest('build/'))
});

gulp.task('build:remove', ['build:copy'], function() {
    del([
        'build/scss',
        'build/css/!(*.min.css)',
        'build/css/bootstrap-theme.min.css',
        'build/scripts',
        'build/js/!(*.min.js)',
        'build/*.html'
    ])
});

////////////////////////////////////////////////////////
// set default build task
////////////////////////////////////////////////////////

gulp.task('build', ['build:copy', 'build:remove', 'build:serve']);



////////////////////////////////////////////////////////
// watch task
////////////////////////////////////////////////////////

gulp.task('watch', function(){

    // watch and compile scss/css
    gulp.watch('app/scss/**/*.scss', ['sass']);

    // watch and compile js
    gulp.watch('app/scripts/**/*.js', ['js']);

    // watch php/html
    gulp.watch('app/**/*.php', ['html']);

});

////////////////////////////////////////////////////////
// set default task
////////////////////////////////////////////////////////

gulp.task('default', ['sass', 'bootstrap', 'bootstrap:copy', 'js', 'jquery', 'html', 'watch', 'browser-sync'] );