<script>
if(typeof(Storage) !== "undefined") {
    if(localStorage.getItem("time") === null){
        // Store
        localStorage.setItem("time", Date.now());
    }

    if(localStorage.getItem("popupshow") === null){
        localStorage.setItem("popupshow", true);
    }
    else{
        console.log("popupshow exists");
    }

    console.log(localStorage.getItem("time"));
    console.log(Math.round(((Date.now()-localStorage.getItem("time"))/1000)));
    
    /*
    if(localStorage.getItem('popupshow') !== "false"){
        var popupfunc = setInterval(function(){
            
            if(Date.now()-localStorage.getItem("time") >= 30000){
                $(function() {
                    $( "#dialog-confirm" ).dialog({
                      resizable: false,
                      modal: true,
                      buttons: {
                        "Formular anzeigen": function() {
                          window.location.href = "https://80.66.224.159/Deep/WebSelfcare/#/explore/home";
                        },
                        "Schliessen": function() {
                          $( this ).dialog( "close" );
                        }
                      }
                    });
                });
                localStorage.setItem("popupshow", false);
                clearInterval(popupfunc);
                console.log(Math.round(((Date.now()-localStorage.getItem("time"))/1000)));
                console.log("done");
            }
            else{
                // Display Timer
                console.log(Math.round(((Date.now()-localStorage.getItem("time"))/1000)));
            }
                
        }, 1000);
    }
    */
    
}
if(typeof(Storage) == "undefined") {
    console.log("Browser untestützt local Storage nicht.")
}

    function addLoadEvent(func) {
        var oldonload = window.onload;
        if(typeof window.onload != 'function'){
            window.onload = func;
        }
        else{
            window.onload = function(){
                oldonload();
                func();
            }
        }
    }
    //Browser outdated style
    addLoadEvent(
        outdatedBrowser({
            bgColor: '#f25648',
            color: '#ffffff',
            lowerThan: 'transform',
            languagePath: 'scripts/outdated-browser-master/outdatedbrowser/lang/de.html'
        })
    );

</script>

<div id="outdated"></div>
<div id="dialog-confirm" style="display: none;" title="Umfrage">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Bitte das Formular ausfüllen</p>
</div>