<?php require('includes/header.php') ?>

<!-- Füge hier dein Html-Markup ein. -->

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2>Füge hier dein Html-Markup ein.</h2>
			<input type="button" id="clear-storage" value="Storage leeren und Seite neu laden" onclick="localStorage.clear(); location.reload();">
        </div>
        <div class="col-xs-12 box-wrap">
        	<div class="red-wrap col-md-2">
        		<div class="red-box" style="background-color: red; width: 100px; height: 100px;"></div>
        	</div>
        	<div class="green-wrap col-md-8">
        		<div class="green-box" style="background-color: green; width: 100px; height: 100px;"></div>
        	</div>
        	<div class="blue-wrap col-md-2">
        		<div class="blue-box" style="background-color: blue; width: 100px; height: 100px;"></div>
        	</div>
        </div>
    </div>
</div>


<?php require('includes/footer.php') ?>
