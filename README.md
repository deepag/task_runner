# README #

### What is this repository for? ###

* Gulp Taskrunner serves to improve and accelerate frontend development.
* v1.0
* More info: https://dark_confidant@bitbucket.org/dark_confidant/gulp_taskrunner.git

### How do I get set up? ###

* Make sure you have installed globally:

    nodeJS v.0.12.7 and higher
    --> https://nodejs.org/en/

    (Once you downloaded nodeJS from the website it's recommended to progress working with the terminal/cmd prompt.


    NodeJS enables the use of node package manager (npm)!


    Open the terminal/cmd prompt and install gulp globally:

    npm install -g gulp
    for more info visit: https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md

    Next up install bower package manager globally:

    npm install -g bower

* If you want to make sure you have installed all prerequisites listed above enter following commands into the terminal/cmd prompt:

    bower -v
    gulp -v
    node -v
    (Each command makes your system check if there is a version installed)


* To get going clone this repository in your new project folder.
* Once the download has finished open terminal and cd into your new project folder.
* Run the following commands:

* npm install
    - this grabs the preconfigured packages needed to run Gulp.

* bower install
    - this grabs the preconfigured packages needed for frontend development.

* gulp
    - sets the engine in motion - starts Browsersync, watches your files in the app/ directory, compiles your scss-files to css and more!

* gulp build
    - creates a build directory and grabs the required files from the app/ directory for you - ready for deployment!

  Have fun with this little but awesome tool!


* If you want to view your site from your device or another browser type in the external IP Adress provided by browerSync in the Terminal (http://192.168.x.xx:8080)

- Raphael